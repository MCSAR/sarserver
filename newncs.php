<?php
/*! \file newncs.php
 *
 *  \brief  Add/Modify NCS - Input form
 *
 *  This script puts up a form for entering a new NCS.  There
 *  are two active buttons: "Back to logging" returns to the
 *  logging screen; "Add NCS" passes form data to newncs1.php
 *  for entry into the database.
 *
 *  \param inc Incident currently being handled
 *  \param ncs Currently set net control
 *
 *  \author John J. McDonough, WB8RCR
 *  \date 2013-11-20
 *
 */

include('functions1.inc');

pageHeadF("Edit NCS","newncs","call");

if ( isset($_GET['inc']) )
  {
    /*! Sequence number of the current incident */
    $incnum = $_GET['inc'];
  }
else
  {
    $incnum = 0;
  }
if ( isset($_GET['ncs']) )
  {
    /*! Sequence number of the current incident */
    $oldncs = $_GET['ncs'];
  }
else
  {
    $oldncs = "";
  }


echo "    </div>\n";
echo "    <div>\n";
echo "      <form name=\"newncs\" method=\"post\" action=\"newncs1.php\">\n";
echo "      <input type=\"hidden\" name=\"ncs\" value=\"" . $oldncs . "\">\n";
echo "      <input type=\"hidden\" name=\"inc\" value=\"" . $incnum . "\">\n";
echo "        <center>\n";
echo "          <table>\n";
echo "            <tr>\n";
echo "              <th class=\"r\">Call: </th>\n";
echo "              <td class=\"l\"><input type=\"text\" size=\"8\" name=\"call\"></td>\n";
echo "            </tr>\n";
echo "            <tr>\n";
echo "              <th class=\"r\">Full name: </th>\n";
echo "              <td class=\"l\"><input type=\"text\" size=\"32\" name=\"name\"></td>\n";
echo "            </tr>\n";
echo "            <tr>\n";
echo "              <th class=\"r\">Cell phone number: </th>\n";
echo "              <td class=\"l\"><input type=\"text\" size=\"12\" name=\"phone\"></td>\n";
echo "            </tr>\n";
echo "          </table>\n";
echo "          <table>\n";
echo "            <tr>\n";
echo "              <td><input type=\"submit\" value=\"Back to logging\"onclick=\"this.form.action='log1.php?inc=" . $incnum . "&ncs=" . $oldncs ."'\" / ></td>\n";
echo "              <td><input type=\"submit\" value=\"Add NCS\"></td>\n";
echo "            </tr>\n";
echo "          </table>\n";
echo "        </center>\n";
echo "      </form>\n";
echo "    </div>\n";
echo "  </body>\n</html>\n";


?>