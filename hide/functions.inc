<?php
/*! \file functions.inc
    \brief Functions used by the application

    A collection of functions used by mulriple pages.
*/

//! Return a formatted error message
/*! Message colored depending on severity
 */
function getMsg($msg,$type='info',$prefix=''){
        $Colour='';
        switch($type){
                case 'error':
                        $Colour='red';
                        $prefix = $prefix ? $prefix : _('ERROR') . ' ' ._('Message Report');
                        break;
                case 'warn':
                        $Colour='maroon';
                        $prefix = $prefix ? $prefix : _('WARNING') . ' ' . _('Message Report');
                        break;
                case 'success':
                        $Colour='darkgreen';
                        $prefix = $prefix ? $prefix : _('SUCCESS') . ' ' . _('Report');
                        break;
                case 'info':
                default:
                        $prefix = $prefix ? $prefix : _('INFORMATION') . ' ' ._('Message');
                        $Colour='navy';
        }
        return '<FONT COLOR="' . $Colour . '"><B>' . $prefix . '</B> : ' .$msg . '</FONT>';
}//getMsg

//! Print a formatted error message
function prnMsg($msg,$type='info', $prefix=''){

        echo '<P>' . getMsg($msg, $type, $prefix) . '</P>';

}//prnMsg

function pageHead( $title )
{
	echo "<html><head><title>MCSAR " . $title . "</title>";
	echo "<style type=\"text/css\" media=\"all\">@import \"default.css\";</style>";
	echo "</head><body>\n";
	echo "<div id=\"headarea\">\n";
	echo "<h1>" . $title . "</h1>\n";
}//pageHead

?>
