<?php
/*! incident.php
 *
 */

/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/

include('functions1.inc');
pageHead("Summary of Incidents");

$db=mysql_connect("localhost","mcsar_user","sarpassword");
mysql_select_db("mcsar_search",$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}

$SQL1="SELECT `incident`,`title`,`startdate`,`enddate` FROM `incident` " .
  "ORDER BY `startdate` DESC";
$result= mysql_query($SQL1,$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}
echo "<center>\n";
echo "<table width=\"75%\">\n";
echo "  <tr>\n";
echo "  <th colspan='2'>Incident</th>\n";
echo "  <th>Start</th>\n";
echo "  <th>End</th>\n";
echo "  </tr>\n";

while ($row=mysql_fetch_row($result))
  {
    echo "    <tr>\n";
    echo "      <td>#" . $row[0] . "</td>\n";
    echo "      <td>" . $row[1] . "</td>\n";
    echo "      <td>" . substr($row[2],0,10) . "</td>\n";
    echo "      <td>" . substr($row[3],0,10) . "</td>\n";
    echo "    <tr>\n";
  }
echo "</table></center>\n";

echo "</body></html>\n";
?>