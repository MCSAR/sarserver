<?php
/**
 * Previous Search Menu
 *
 * @author John J. McDonough, WB8RCR <wb8rcr@arrl.net>
 *
 */
$incnum = $_GET['inc'];
echo "<html>\n";
echo "<head>\n";
echo "<title>MCSAR Previous Search Menu</title>\n";
echo "<style type=\"text/css\" media=\"all\">@import \"default1.css\";</style>\n";
echo "</head>\n";
include('functions.inc');

// Open the database
$db=mysql_connect("localhost","mcsar_user","sarpassword");
mysql_select_db("mcsar_search",$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}
$title = "";
if ( $incnum > 0 )
  {
    $SQL0 = "SELECT `title` FROM `incident` WHERE `incident`=" . $incnum ;
    $result = mysql_query($SQL0,$db);
    $row=mysql_fetch_row($result);
    $title = $row[0];
  }

echo "<body>\n";
echo "<div id=\"upper\">\n";
if ( $title != "" )
  echo "<h2>" . $title . " Menu</h2>\n";
else
  echo "<h2>Menu</h2>\n";
echo "<center>\n";
echo "<a class=\"menu\" href=\"log1.php?inc=" . $incnum . "\" target=\"2\">\n";
echo "  Make Radio Log Entries\n";
echo "</a><br /><br />\n";
echo "<a class=\"menu\" href=\"logv.php?inc=" . $incnum . "\" target=\"4\">\n";
echo "  View entire log\n";
echo "</a></br /><br />\n";
echo "<a class=\"menu\" href=\"logpdf.php?inc=" . $incnum . "\" target=\"3\">\n";
echo "  Make PDF of log\n";
echo "</a><br /><br />\n";
echo "<a class=\"menu\" href=\"prevsearch.php\" target=\"1\">\n";
echo "  Select Another Search\n";
echo "</a><br /><br />\n";
echo "<a class=\"menu\" href=\"index.html\" >\n";
echo "  Return to current search\n";
echo "</a><br />\n";
echo "<br /><br />\n";
echo "</center>\n";
echo "</div>\n";
echo "</body>\n";
echo "</html>\n";
?>