<?php
/*! \file  logpdf.php
 *
 *  \brief  Generate a pdf of the radio log
 */
include ('class.pdf.php');

/*! Create the initial PDF of the document */
function DocumentSetup($pdf,$Page_Width,$Page_Height,$Top_Margin,$Bottom_Margin,
                       $Left_Margin,$Right_Margin,$Month)
{
  $PageSize = array(0,0,$Page_Width,$Page_Height);
  $pdf = & new Cpdf($PageSize);

  $PageSize = array(0,0,$Page_Width,$Page_Height);
  $pdf = & new Cpdf($PageSize);

  $pdf->addinfo('Author','John J. McDonough ' . "WB8RCR");
  $pdf->addinfo('Creator','logpdf.php $Revision: 1.1$');
  $pdf->SetKeywords('MCSAR, Search and Rescue');
  $pdf->selectFont('helvetica');

  $pdf->addinfo('Title',_('MCSAR Search ') .  $Month  . _(' Log'));
  $pdf->addinfo('Subject',_('Radio Log') );

  return $pdf;

}

/*! Set up a new page */
function NextPage($pdf,$PageNumber,$Page_Width,$Page_Height,$Top_Margin,$Bottom_Margin,
                 $Left_Margin,$Right_Margin,$Month)
{
  $PageNumber++;
  if ($PageNumber>1)
    {
      $pdf->newPage();
    }

  // Pale purple background on printable area
  $pdf->SetFillColor(248,244,255);
  //  $pdf->Rect($Left_Margin,$Top_Margin,$Page_Width-$Left_Margin-$Right_Margin,
  //             $Page_Height-$Top_Margin-$Bottom_Margin,'F');

  // Top line
  $pdf->SetTextColor(0,0,0);
  $pdf->selectFont('helvetica-Bold');
  $FontSize=18;
  $YPos = $Page_Height - $Top_Margin - 19;
  $XPos = $Page_Width/2 - 120;
  $pdf->SetTextColor(0,0,128);
  $pdf->addText($XPos,$YPos,$FontSize,_('Midland County Search and Rescue'));
  $pdf->SetTextColor(0,0,0);
  // Bottom line
  $FontSize=15;
  $YPos = $YPos - 18;
  $XPos = $Page_Width/2 - 80;
  $pdf->addText($XPos,$YPos,$FontSize,$Month);
  
  // Page Number
  $FontSize=8;
  $YPos = $Bottom_Margin + 2;
  if ( $PageNumber & 1 )
    $XPos = $Page_Width - 60;
  else
    $XPos = $Left_Margin + 10;
  $msg = 'Page ' . $PageNumber;
  $pdf->addText($XPos,$YPos,$FontSize,$msg);
  
  $FontSize=10;
  $pdf->selectFont('courier');
}

/*! Place a centered line of text */
function centerText($pdf,$l,$r,$y,$fs,$text)
{
  $slen=$pdf->GetStringWidth($text);
  $XPos = round(($l+$r)/2 - $slen/2 );
  //$msg = 'Len ' . $slen . ', y=' . $y . ', text=' . $text;
  //$pdf->addText(200,300,10,$msg);
  //$msg = 'l,r=' . $l . ',' . $r . ', Pos ' . $XPos;
  //$pdf->addText(200,340,10,$msg);
  $pdf->addText($XPos,$y,$fs,$text);
}


$title=_('Search and Rescue Radio Log');

include('functions.inc');

// Remember the launch time
$starttime = strftime("%A, %B %d %Y, %H:%M");

// Open the database
$db=mysql_connect("localhost","mcsar_user","sarpassword");
mysql_select_db("mcsar_search",$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}

//-------------------------------------------
// Get incident numer from command or most recent
//-------------------------------------------
if ( isset($_GET['inc']) )
  {
    $incnum = $_GET['inc'];
  }
else
  {
    $incnum = 0;
  }
if ( $incnum < 1 )
  {
    $SQL0 = "SELECT MAX(`incident`) FROM `incident`";
    $result = mysql_query($SQL0,$db);
    $row=mysql_fetch_row($result);
    $incnum = $row[0];
  }

$PaperSize = 'letter';
$Page_Width=612;
$Page_Height=792;
$Top_Margin=30;
$Bottom_Margin=57;
$Left_Margin=30;
$Right_Margin=25;

$pdf = DocumentSetup($pdf,$Page_Width,$Page_Height,$Top_Margin,$Bottom_Margin,
                     $Left_Margin,$Right_Margin,$incnum);

$PageNumber = 0;

$SQL0 = "SELECT `title` FROM `incident` WHERE `incident`=" . $incnum . ";";
$r0 = mysql_query($SQL0,$db);
$ro0 = mysql_fetch_row($r0);
$subtitle = "Radio log for " . $ro0[0];

$SQL = "SELECT `contacttime`,`team`,`callsign`,`par`,`sector`,`notes`,`NCScall` " .
  " FROM `log` " .
  " WHERE `incident`=" . $incnum .
  " ORDER BY `contacttime`;";
$res=mysql_query($SQL, $db);
$YPos = 0;
while ( $row = mysql_fetch_row($res) )
  {
    if ( $YPos < 100 )
      {
	NextPage($pdf,$PageNumber,$Page_Width,$Page_Height,
		 $Top_Margin,$Bottom_Margin,
		 $Left_Margin,$Right_Margin,$Month);
	$pdf->addJpegFromFile('/var/www/html/sar/MCSAR-small.jpg',$Left_Margin,
			      $Page_Height-$Top_Margin-50,
			      70,70);
	$pdf->SelectFont("helvetica-Bold");
	$FontSize = 12;
	centerText($pdf,$Left_Margin,$Page_Width-$Right_Margin,
		   $Page_Height-$Top_Margin-40,$FontSize,
		   $subtitle);
	$YPos = $Page_Height-$Top_Margin-70;
	$pdf->addText($Left_Margin+40,$YPos,$FontSize,"Time");
	$pdf->addText($Left_Margin+110,$YPos,$FontSize,"Team");
	$pdf->addText($Left_Margin+165,$YPos,$FontSize,"Call");
	$pdf->addText($Left_Margin+205,$YPos,$FontSize,"Par");
	$pdf->addText($Left_Margin+230,$YPos,$FontSize,"Sector");
	$pdf->addText($Left_Margin+290,$YPos,$FontSize,"Notes");
	$pdf->addText($Left_Margin+500,$YPos,$FontSize,"NCS");
	$pdf->SetDrawColor(96,112,128);
	$pdf->Line( $Left_Margin, $YPos-3, 
		    $Page_Width-$Left_Margin-$Right_Margin,$YPos-3);
	$pdf->SetDrawColor(0,0,0);
	$YPos -= 14;
	$pdf->SelectFont("helvetica");
	$FontSize = 10;
	$NCSfont = 8;
	$PageNumber++;
      }
    //    $pdf->Line( $Left_Margin, $YPos, 
    //		$Page_Width-$Left_Margin-$Right_Margin,
    //		$YPos);
    $pdf->addText($Left_Margin+10,$YPos,$FontSize,$row[0]);
    $pdf->addText($Left_Margin+120,$YPos,$FontSize,$row[1]);
    $pdf->addText($Left_Margin+160,$YPos,$FontSize,$row[2]);
    $pdf->addText($Left_Margin+210,$YPos,$FontSize,$row[3]);
    $pdf->addText($Left_Margin+240,$YPos,$FontSize,$row[4]);
    $words = explode(" ",$row[5],200);
    $i = 0;
    $string = "";
    while ( $words[$i] )
      {
	if ( strlen($string) > 34 )
	  {
	    $pdf->addText($Left_Margin+290,$YPos,$FontSize,$string);
	    $string = "";
	    $YPos -= 14;
	  }
	$string = $string . $words[$i] . " ";
	//$pdf->addText($Left_Margin+290,$YPos,$FontSize,$string);
	//$YPos -= 14;
	$i++;
      }
    $pdf->addText($Left_Margin+290,$YPos,$FontSize,$string);
    $pdf->addText($Left_Margin+490,$YPos,$NCSfont,$row[6]);
    $pdf->SetDrawColor(192,224,255);
    $pdf->Line( $Left_Margin, $YPos-3, 
		$Page_Width-$Left_Margin-$Right_Margin,$YPos-3);
    $pdf->SetDrawColor(0,0,0);
    $YPos -= 14;
  }

$buf = $pdf->output();
$len += strlen($buf);

header('Content-type: application/pdf');
header('Content-Length: ' . $len);
header('Content-Disposition: inline; filename=' . $DateShort . 'MCSARlog.pdf');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');

$pdf->stream()

?>