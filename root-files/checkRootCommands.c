#include <stdio.h>
#include <string.h>

#define DATEFILE "/var/www/html/sar/timetoset"
#define DOWNFILE "/var/www/html/sar/shutdown"
#define LOGFILE "/root/commands.log"

char szBuffer[1024];
char szCommand[1024];

void logit( char *szCommand )
{
  FILE *g;

  fprintf(stderr,"checkRootCommands: Command=[%s]\n",szCommand);
  g = fopen(LOGFILE,"w");
  fprintf(g,"Cmd:[%s]\n",szCommand);
  fclose(g);
}

void main()
{
  FILE *f;

  while (1) 
    {
      f = fopen(DATEFILE,"r");
      if ( f )
	{
	  fgets(szBuffer,sizeof(szBuffer),f);
	  strcpy(szCommand,"date -s '");
	  strcat(szCommand,szBuffer);
	  strcat(szCommand,"'");
	  fclose(f);
	  logit(szCommand);
	  unlink(DATEFILE);
	  system(szCommand);
	}
      //      else
      //	perror("Datefile");
      f = fopen(DOWNFILE,"r");
      if ( f )
	{
	  fclose(f);
	  unlink(DOWNFILE);
	  strcpy(szCommand,"shutdown -h now");
	  logit(szCommand);
	  system(szCommand);
	}
      //      else
      //	perror("Downfile");

      //      fprintf(stderr,"Sleeping\n");
      sleep(10);
    }
}
