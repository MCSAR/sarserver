<?php
/*! \file shutdown.php
 *
 *  \brief  Shut down the system
 *
 * @author John J. McDonough, WB8RCR <wb8rcr@arrl.net>
 *
 */

include('functions1.inc');
pageHead("Shut down the SAR server");

echo "<h1>The system should shut down shortly</h1>\n";

echo "  </body>\n</html>\n";
file_put_contents("shutdown","shutdown -h now");

?>
