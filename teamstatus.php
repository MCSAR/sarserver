<?php
  /*! \file logv.php

  \brief View entireRadio log

  */
include('functions1.inc');
pageHeadR("Team Status");
//-------------------------------------------
// Open connection to database
//-------------------------------------------
$db=mysql_connect("localhost","mcsar_user","sarpassword");
mysql_select_db("mcsar_search",$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}

date_default_timezone_set('America/Detroit');

//-------------------------------------------
// Get incident numer from command or most recent
//-------------------------------------------
if ( isset($_GET['inc']) )
  {
    $incnum = $_GET['inc'];
  }
else
  {
    $incnum = 0;
  }
if ( $incnum < 1 )
  {
    $SQL0 = "SELECT MAX(`incident`) FROM `incident`";
    $result = mysql_query($SQL0,$db);
    $row=mysql_fetch_row($result);
    $incnum = $row[0];
  }

//-------------------------------------------
// Display the incident title
//-------------------------------------------
echo "  <div id=\"hintarea\">\n";
echo "    \n";
echo "  </div>\n";

echo "  <div id=\"freqarea\">\n";
echo "    <p style=\"color:lawngreen; font-size: 8pt; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: right;\">\n";
echo "      updated " . strftime('%H:%M') . "Z\n";
echo "    </p>\n";
echo "  </div>\n";

$SQL1="SELECT`title` FROM `incident` " .
  "WHERE `incident`=" . $incnum;
$result= mysql_query($SQL1,$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}
$row=mysql_fetch_row($result);
echo "<h3>" . $row[0] . "</h3>\n";
echo "</div>\n";


//-------------------------------------------
// Display older log entries for this incident
//-------------------------------------------
echo "<div id=\"upperl\">\n";
//echo "<div id=\"logsum\">\n";
echo "<center>\n";

//==================================================
$SQL8="SELECT DISTINCT `team`  FROM `log` WHERE `incident`=" . $incnum . " ORDER BY `contacttime` DESC;";
$res8 = mysql_query($SQL8,$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}
//echo "<p>============================<br />\n";
//echo $SQL8 . "<br />\n";
echo "<table width=\"95%\">\n";
echo "<tr><th>Time</th><th>Team</th><th>Sector</th><th>Call</th><th>Par</th><th>Last Note</th></tr>\n";
while ( $row8 = mysql_fetch_row( $res8 ) )
  {
    echo "<tr>\n";
    //echo $row8[0] . "<br />\n";
    $SQL9 = "SELECT `contacttime`,`callsign`,`par`,`sector`,`notes` FROM `log` " .
      "WHERE `incident`=" . $incnum . " AND `team`='" . $row8[0] . "' " .
      "ORDER BY `contacttime` DESC";
    $res9 = mysql_query($SQL9,$db);
    $row9 = mysql_fetch_row($res9);
    //echo $row9[0] . "|" . $row9[1] . "|" . $row9[3] . "<br />\n";
    $color = setBackground( $row9[0], $row9[3], $row8[0] );
    if ( $color=="" )
      echo "  <td>" . $row9[0] . "</td>\n";
    else
      echo "  <td class=\"" . $color . "\">" . $row9[0] . "</td>\n";
    echo "  <td class=\"full\">" . $row8[0] . "</td>\n";
    if ( ($row9[3]=="Stag") || $row9[3]=="STAG" )
      echo "  <td class=\"stage\">Staging</td>\n";
    else
      echo "  <td class=\"full\">" . $row9[3] . "</td>\n";
    echo "  <td>" . $row9[1] . "</td>\n";
    echo "  <td>" . $row9[2] . "</td>\n";
    echo "  <td class=\"note\">" . $row9[4] . "</td>\n";
    echo "</tr>\n";
  }
echo "</table>\n";
  echo "</p>\n";
//==================================================

echo "</center>\n";
echo "</div>\n";

//echo "</div>\n";
echo "</form>\n";
echo "</body></html>\n";
?>