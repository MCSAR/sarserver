<?php
  /*! \file logv.php

  \brief View entireRadio log

  */
include('functions1.inc');
pageHeadR("Live Radio Log");
//-------------------------------------------
// Open connection to database
//-------------------------------------------
$db=mysql_connect("localhost","mcsar_user","sarpassword");
mysql_select_db("mcsar_search",$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}

date_default_timezone_set('America/Detroit');

//-------------------------------------------
// Get incident numer from command or most recent
//-------------------------------------------
if ( isset($_GET['inc']) )
  {
    $incnum = $_GET['inc'];
  }
else
  {
    $incnum = 0;
  }
if ( $incnum < 1 )
  {
    $SQL0 = "SELECT MAX(`incident`) FROM `incident`";
    $result = mysql_query($SQL0,$db);
    $row=mysql_fetch_row($result);
    $incnum = $row[0];
  }

//-------------------------------------------
// Display the incident title
//-------------------------------------------
echo "  <div id=\"hintarea\">\n";
echo "    \n";
echo "  </div>\n";

echo "  <div id=\"freqarea\">\n";
echo "    <p style=\"color:lawngreen; font-size: 8pt; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: right;\">\n";
echo "      updated " . strftime('%H:%M') . "Z\n";
echo "    </p>\n";
echo "  </div>\n";

$SQL1="SELECT`title` FROM `incident` " .
  "WHERE `incident`=" . $incnum;
$result= mysql_query($SQL1,$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}
$row=mysql_fetch_row($result);
echo "<h3>" . $row[0] . "</h3>\n";
echo "</div>\n";

//-------------------------------------------
// Display older log entries for this incident
//-------------------------------------------
echo "<div id=\"upperl\">\n";
//echo "<div id=\"logsum\">\n";
echo "<center>\n";

$SQL7 = "SELECT COUNT(*) FROM  `log` WHERE `incident`=" . $incnum;
$res1 = mysql_query($SQL7,$db);
$cnt = mysql_fetch_row( $res1 );
$SQL2="SELECT `contacttime`,`team`,`callsign`,`par`,`sector`,`notes` " .
  "FROM `log` WHERE `incident`=" . $incnum .
  " ORDER BY `contacttime` DESC";
$result= mysql_query($SQL2,$db);

echo "<table width=\"99%\">\n";
echo "  <tr>\n";
echo "    <th>Time</th>\n";
echo "    <th>Team</th>\n";
echo "    <th>Call</th>\n";
echo "    <th>PAR</th>\n";
echo "    <th>Sector</th>\n";
echo "    <th>Notes</th>\n";
echo "  </tr>\n";
$olddate = "";

while ( $row=mysql_fetch_row($result) )
  {
    if ( strcmp($olddate,substr($row[0],0,10)) )
      {
        $olddate = substr($row[0],0,10);
        echo "  <tr>\n";
        echo "    <th colspan=\"6\" class=\"date\">" . substr($row[0],0,10) . "</th>\n";

        echo "  </tr>\n";
      }
    echo "  <tr>\n";
    echo "    <td class=\"full\">" . substr($row[0],11,5) . "</td>\n";
    echo "    <td class=\"full\">" . $row[1] . "</td>\n";
    echo "    <td class=\"full\">" . $row[2] . "</td>\n";
    echo "    <td class=\"full\">" . $row[3] . "</td>\n";
    echo "    <td class=\"full\">" . $row[4] . "</td>\n";
    echo "    <td class=\"note\">" . $row[5] . "</td>\n";
    echo "  </tr>\n";
  }
echo "</table>\n";
echo "</center>\n";
echo "</div>\n";

//echo "</div>\n";
echo "</form>\n";
echo "</body></html>\n";
?>