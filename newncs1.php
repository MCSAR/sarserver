<?php
/*! \file newncs1.php
 *
 *  \brief  Add/Modify NCS - database update
 *
 *  \author John J. McDonough, WB8RCR
 *  \date 2013-11-20
 *
 */

include('functions1.inc');
pageHeadF("Edit NCS","newncs","call");

if ( isset($_POST['inc']) )
  {
    /*! Sequence number of the current incident */
    $incnum = $_POST['inc'];
  }
else
  {
    $incnum = 0;
  }
if ( isset($_POST['ncs']) )
  {
    /*! Callsign of the NCS making the change */
    $oldncs = $_POST['ncs'];
  }
else
  {
    $oldncs = "";
  }

if ( isset($_POST['call']) )
  {
    /*! Callsign of the NCS to be added */
    $call = $_POST['call'];
  }
else
  {
    $call = "";
  }

if ( isset($_POST['name']) )
  {
    /*! Full name of the new NCS to be added */
    $name = $_POST['name'];
  }
else
  {
    $name = "";
  }

if ( isset($_POST['phone']) )
  {
    /*! Cell phone number of the new NCS to be added */
    $phone = $_POST['phone'];
  }
else
  {
    $phone = "";
  }

//-------------------------------------------
// Open connection to database
//-------------------------------------------
/*! Database handle */
$db=mysql_connect("localhost","mcsar_user","sarpassword");
mysql_select_db("mcsar_search",$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}


echo "    </div>\n";
echo "    <div>\n";
echo "      <p>Incident=" . $incnum . "</p>\n";
echo "      <p>Old NCS=" . $oldncs . "</p>\n";
echo "      <p>&nbsp;</p>\n";
echo "      <p>Call=" . $call . "</p>\n";
echo "      <p>Name=" . $name . "</p>\n";
echo "      <p>Phone=" . $phone . "</p>\n";

/* Require a reasonable call sign */
if ( strlen($call) < 4 )
  header("Location: newncs.php?inc=" . $incnum . "&ncs=" . $oldncs );

/* Require some operator name */
if ( strlen($name) < 4 )
  header("Location: newncs.php?inc=" . $incnum . "&ncs=" . $oldncs );

/*! Call signs always all upper case */
$call=strtoupper($call);
echo "      <p>&nbsp;</p>\n";
echo "      <p>Call=" . $call . "</p>\n";

/*! SQL query to check whether already in database */
$SQL1="SELECT * FROM `NCSops` WHERE `call`='" . $call . "';";
echo "      <p>" . $SQL1 . "</p>\n";

/*! Result of query to check if in database */
$res1=mysql_query($SQL1,$db);

/*! Number of times call already in the database */
if ($row1=mysql_fetch_row($res1) )
  {
    /* User already in database */
    /* Check if phone changed */
    if ( strlen($phone)>6 )
      {
	if ( $phone != $res1[2] )
	  {
	    /*! Query to update phone number */
	    $SQL2="UPDATE `NCSops` SET `cell`='" . $phone . "' WHERE " .
	      "`call`='" . $call . "';";
	    /*! Execute query to update phone number */
	    $res2=mysql_query($SQL2,$db);
	  }
      }
    if ( strlen($name)>4 ) /* Look for a reasonable name */
      {
	if ( $name != $res1[1] )
	  {
	    /*! Query to update name */
	    $SQL3="UPDATE `NCSops` SET `name`='" . $name . "' WHERE " .
	      "`call`='" . $call . "';";
	    /*! Execute query to update name */
	    $res3=mysql_query($SQL3,$db);
	  }
      }
  }
else
  {
    /*! Text to use for phone in INSERT statement */
    $phonefield = "'" . $phone . "'";
    if ( strlen($phone)<1 )
      $phonefield="NULL";
    /*! Query to insert new net control */
    $SQL4="INSERT INTO `NCSops` VALUES('" . $call . "','" .
      $name . "'," . $phonefield . ");";
    echo "<p>" . $SQL4 . "</p>\n";
    /*! Execute query to insert new net control */
    $res4=mysql_query($SQL4,$db);
  }


  header("Location: log1.php?inc=" . $incnum . "&ncs=" . $oldncs );
?>