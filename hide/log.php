<?php
include('functions.inc');
pageHead("Radio Log");

//-------------------------------------------
// Open connection to database
//-------------------------------------------
$db=mysql_connect("localhost","jjmcd","alskdj");
mysql_select_db("mcsar_search",$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}

//-------------------------------------------
// Get incident numer from command or most recent
//-------------------------------------------
$incnum = $_GET['inc'];
if ( $incnum < 1 )
  {
    $SQL0 = "SELECT MAX(`incident`) FROM `incident`";
    $result = mysql_query($SQL0,$db);
    $row=mysql_fetch_row($result);
    $incnum = $row[0];
  }

//-------------------------------------------
// Display the incident title
//-------------------------------------------
$SQL1="SELECT`title` FROM `incident` " .
  "WHERE `incident`=" . $incnum;
$result= mysql_query($SQL1,$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}
$row=mysql_fetch_row($result);
echo "<h2><center>" . $row[0] . "</center></h2>\n";
echo "<center>\n";

//-------------------------------------------
// Display older log entries for this incident
//-------------------------------------------
$SQL2="SELECT `contacttime`,`team`,`callsign`,`par`,`sector`,`notes` " .
  "FROM `log` WHERE `incident`=" . $incnum .
  " ORDER BY `contacttime`";
$result= mysql_query($SQL2,$db);


echo "<table width=\"90%\">\n";
echo "  <tr>\n";
echo "    <th>Time</th>\n";
echo "    <th>Team</th>\n";
echo "    <th>Call</th>\n";
echo "    <th>PAR</th>\n";
echo "    <th>Sector</th>\n";
echo "    <th>Notes</th>\n";
echo "  </tr>\n";
$olddate = "";

while ( $row=mysql_fetch_row($result) )
  {
    if ( strcmp($olddate,substr($row[0],0,10)) )
      {
	$olddate = substr($row[0],0,10);
	echo "  <tr>\n";
	echo "    <th colspan=\"6\" class=\"date\">" . substr($row[0],0,10) . "</th>\n";
	echo "  </tr>\n";
      }
    echo "  <tr>\n";
    echo "    <td>" . substr($row[0],11,5) . "</td>\n";
    echo "    <td>" . $row[1] . "</td>\n";
    echo "    <td>" . $row[2] . "</td>\n";
    echo "    <td>" . $row[3] . "</td>\n";
    echo "    <td>" . $row[4] . "</td>\n";
    echo "    <td class=\"note\">" . $row[5] . "</td>\n";
    echo "  </tr>\n";
  }
echo "</table>\n";
echo "<p>&nbsp;</p>\n";

//-------------------------------------------
// Display team statuses
//-------------------------------------------
$SQL5="SELECT `team`,`lastcontacttime`,`callsign`,`par`,`sector` " .
  "FROM `team` ORDER BY `lastcontacttime`";
$result= mysql_query($SQL5,$db);
echo "<table width=\"90%\">\n";
echo "  <tr>\n";
echo "    <th>Team</th>\n";
echo "    <th>Last Contact</th>\n";
echo "    <th>Call sign</th>\n";
echo "    <th>PAR</th>\n";
echo "    <th>Sector</th>\n";
echo "  </tr>\n";
while ( $row=mysql_fetch_row($result) )
  {
    echo "  <tr>\n";
    echo "    <td>" . $row[0] . "</td>\n";
    if ( substr($row[1],11,5) < "17:00" )
      echo "    <td class=\"warn\">" . substr($row[1],11,5) . "</td>\n";
    else
      echo "    <td>" . substr($row[1],11,5) . "</td>\n";
    echo "    <td>" . $row[2] . "</td>\n";
    echo "    <td>" . $row[3] . "</td>\n";
    echo "    <td>" . $row[4] . "</td>\n";
    echo "  </tr>\n";
  }

echo "</table>\n";
echo "<p>&nbsp;</p>\n";

//-------------------------------------------
// Allow input of new row
//-------------------------------------------
echo "<table width=\"90%\">\n";
echo "  <tr>\n";
echo "    <th>Team</th>\n";
echo "    <th>Call</th>\n";
echo "    <th>PAR</th>\n";
echo "    <th>Sector</th>\n";
echo "    <th>Notes</th>\n";
echo "  </tr>\n";
echo "  <tr>\n";
echo "    <td><input type=\"text\" size=\"4\"></td>\n";
echo "    <td><input type=\"text\" size=\"8\"></td>\n";
echo "    <td><input type=\"text\" size=\"3\"></td>\n";
echo "    <td><input type=\"text\" size=\"3\"></td>\n";
echo "    <td><input type=\"text\" size=\"50\"></td>\n";
echo "    <td><input type=\"submit\"></td>\n";
echo "  </tr>\n";



echo "</table>\n";
echo "</center>\n";
echo "</body></html>\n";
?>