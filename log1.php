<?php
/*! \file log1.php
 *
 *  \brief  Radio Log Entry
 *
 * @author John J. McDonough, WB8RCR <wb8rcr@arrl.net>
 *
 */

include('functions1.inc');
pageHeadF("Log Entry","logentry","team");
      
//-------------------------------------------
// Open connection to database
//-------------------------------------------
/*! Database handle */
$db=mysql_connect("localhost","mcsar_user","sarpassword");
mysql_select_db("mcsar_search",$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),'error', _('Database Error')); 
}

date_default_timezone_set('America/Detroit');

//-------------------------------------------
// Get incident number from command or most recent
//-------------------------------------------
if ( isset($_GET['inc']) )
  {
    /*! Sequence number of the current incident */
    $incnum = $_GET['inc'];
  }
else
  {
    $incnum = 0;
  }
if ( isset($_GET['ncs']) )
  {
    /*! Sequence number of the current incident */
    $ncs = $_GET['ncs'];
  }
else
  {
    $ncs = "";
  }
if ( isset($_POST['ncscall']) )
  {
    /*! Sequence number of the current incident */
    $ncs = $_POST['ncscall'];
  }
// Not passed so find highest number in the incident table
if ( $incnum < 1 )
  {
    /*! SQL statement to find highest incident number */
    $SQL0 = "SELECT MAX(`incident`) FROM `incident`";
    /*! Result of SQL query for highest incident number */
    $result = mysql_query($SQL0,$db);
    /*! Row of query return */
    $row=mysql_fetch_row($result);
    $incnum = $row[0];
  }

//-------------------------------------------
// Display the incident title
//-------------------------------------------
echo "      <div id=\"hintarea\">\n";
echo "        <p>--</p>\n";
echo "      </div> <!-- hintarea -->\n";
echo "      <div id=\"freqarea\">\n";
echo "        <ul style=\"color:lawngreen; font-size: 8pt; font-family: Verdana, Arial, Helvetica, sans-serif;\">\n";
echo "          <li>Main - 146.445</li>\n";
echo "          <li>Medical - 147.445</li>\n";
echo "          <li>Admin - 146.415</li>\n";
echo "          <li>Overflow - 147.510</li>\n";
echo "        </ul>\n";
echo "      </div> <!-- freqarea -->\n";
$SQL1="SELECT`title` FROM `incident` " .
  "WHERE `incident`=" . $incnum;
$result= mysql_query($SQL1,$db);
if (mysql_errno($db) != 0 )
{
    prnMsg($ErrorMessage.'<BR>' . mysql_error($db),
	   'error', _('Database Error')); 
}
$row=mysql_fetch_row($result);
echo "      <h3>" . $row[0] . "</h3>\n";
echo "    </div> <!-- headarea -->\n";


//-------------------------------------------
// Display older log entries for this incident
//-------------------------------------------
echo "    <div id=\"upper\">\n";
echo "      <div id=\"logsum\">\n";
echo "        <center>\n";

/* Get the count of log entries */
$SQL7 = "SELECT COUNT(*) FROM  `log` WHERE `incident`=" . $incnum;
$res1 = mysql_query($SQL7,$db);
$cnt = mysql_fetch_row( $res1 );
/* SELECT for the log might be limited to number of entries */
$SQL2="SELECT `contacttime`,`team`,`callsign`,`par`,`sector`,`notes` " .
  "FROM `log` WHERE `incident`=" . $incnum .
  " ORDER BY `contacttime`";
// Limit to 20 most recent entries
if ( $cnt[0] > 20 )
  {
    $scnt = $cnt[0] - 20;
    $SQL2 = $SQL2 . " LIMIT " . $scnt . "," . $cnt[0];
  }
$result= mysql_query($SQL2,$db);
/* Table headings */
echo "          <table width=\"99%\">\n";
echo "            <tr>\n";
echo "              <th>Time</th>\n";
echo "              <th>Team</th>\n";
echo "              <th>Call</th>\n";
echo "              <th>PAR</th>\n";
echo "              <th>Sector</th>\n";
echo "              <th>Notes</th>\n";
echo "            </tr>\n";
$olddate = "";
/* Now loop through the log entries displaying one per row */
while ( $row=mysql_fetch_row($result) )
  {
    if ( strcmp($olddate,substr($row[0],0,10)) )
      {
	$olddate = substr($row[0],0,10);
	echo "            <tr>\n";
	echo "              <th colspan=\"6\" class=\"date\">" . 
	  substr($row[0],0,10) .  "</th>\n";
	echo "            </tr>\n";
      }
    echo "            <tr>\n";
    echo "              <td>" . substr($row[0],11,5) . "</td>\n";
    echo "              <td>" . $row[1] . "</td>\n";
    echo "              <td>" . $row[2] . "</td>\n";
    echo "              <td>" . $row[3] . "</td>\n";
    echo "              <td>" . $row[4] . "</td>\n";
    echo "              <td class=\"note\">" . $row[5] . "</td>\n";
    echo "            </tr>\n";
  }
echo "          </table>\n";
echo "        </center>\n";
echo "      </div> <!-- logsum -->\n";


//-------------------------------------------
// Display team statuses
//-------------------------------------------
echo "      <div id=\"teamsum\">\n";
echo "        <center>\n";
$SQL5="SELECT `team`,`lastcontacttime`,`callsign`,`par`,`sector` " .
  "FROM `team` WHERE `incident`=" . $incnum . " ORDER BY `lastcontacttime`";
$result= mysql_query($SQL5,$db);
echo "          <table width=\"99%\">\n";
echo "            <tr>\n";
echo "              <th>Team</th>\n";
echo "              <th>Last Contact</th>\n";
echo "              <th>Call sign</th>\n";
echo "              <th>PAR</th>\n";
echo "              <th>Sector</th>\n";
echo "            </tr>\n";
while ( $row=mysql_fetch_row($result) )
  {
    // Calculate time since last contact
    $then = strtotime( $row[1] );
    $now = mktime();
    $seconds = $now - $then;
    if ( $seconds < 60000 )
      {
	echo "            <tr>\n";
	echo "              <td>" . $row[0] . "</td>\n";
	$color = setBackground( $row[1], $row[4], $row[0] );
	if ( $color == "" )
	  echo "              <td>" . substr($row[1],11,5) . " (" . 
	    floor($seconds/60) .  ")</td>\n";
	else
	  echo "              <td class=\"" . $color . "\">" . 
	    substr($row[1],11,5) . " (" . floor($seconds/60) . ")</td>\n";
	echo "              <td>" . $row[2] . "</td>\n";
	echo "              <td>" . $row[3] . "</td>\n";
	echo "              <td>" . $row[4] . "</td>\n";
	echo "            </tr>\n";
      }
  }

echo "          </table>\n";
echo "        </center>\n";
//echo "<p>&nbsp;</p>\n";
echo "      </div> <!-- teamsum -->\n";
echo "    </div> <!-- upper -->\n";

//-------------------------------------------
// Allow input of new log entry
//-------------------------------------------
echo "    <br />\n";
echo "    <div id=\"entarea\">\n";
echo "      <form name=\"logentry\" method=\"post\" action=\"logput.php?inc=" . 
    $incnum . "&ncs=" . $ncs . "\">\n";
echo "        <center>\n";
echo "          <table width=\"90%\">\n";
echo "            <tr>\n";
echo "              <th>Team</th>\n";
echo "              <th>Call</th>\n";
echo "              <th>PAR</th>\n";
echo "              <th>Sector</th>\n";
echo "              <th>Notes</th>\n";
if ( $ncs == "" )
  echo "              <td style=\"color: red; font-size: 16pt;\"><b>NO NCS</b></td>\n";
else
  echo "              <td>" . $ncs . "</td>\n";
echo "            </tr>\n";
echo "            <tr>\n";
echo "              <td><input type=\"text\" size=\"4\" name=\"team\"></td>\n";
echo "              <td><input type=\"text\" size=\"8\" name=\"call\"></td>\n";
echo "              <td><input type=\"text\" size=\"3\" name=\"par\"></td>\n";
echo "              <td><input type=\"text\" size=\"3\" name=\"sector\"></td>\n";
echo "              <td><input type=\"text\" size=\"50\" name=\"notes\"></td>\n";
echo "              <td><input type=\"submit\"></td>\n";
echo "            </tr>\n";
echo "          </table>\n";
echo "        </center>\n";
//-------------------------------------------
// Reminder text of periodic announcement
//-------------------------------------------
echo "        <p class=\"announce\">This frequency is in " .
    "use by the Midland County Search and Rescue Team which is holding a " .
    "directed net on this frequency.  A search is in progress. All " .
    "broadcasts on this frequency should be directed to net control.</p>\n";
echo "      </form>\n";
echo "    </div> <!-- entarea -->\n";
//-------------------------------------------
// Allow for changing NCS
//-------------------------------------------
echo "    <br />\n";
echo "    <form name=\"changencs\" method=\"post\" action=\"log1.php?inc=" . 
    $incnum . "&ncs=" . $ncs . "\" >\n";
echo "      <table>\n        <tr>\n          <th>\n";
echo "            <p>&nbsp;</p><p>Change NCS:</p>\n";
echo "            <select name=\"ncscall\">\n";
$SQL6 = "SELECT `call` FROM `NCSops`  ORDER BY `call`";
$res6 = mysql_query($SQL6,$db);
while ( $row6=mysql_fetch_row($res6) )
  {
    echo "              <option";
    if ( $ncs == $row6[0] )
      echo " selected=\"selected\"";
    echo ">" . $row6[0] . "</option>\n";
  }
echo "            </select>\n";
echo "          </th>\n          <td><input type=\"submit\" value=\"Submit\"></td>\n        </tr>\n      </table>\n";
echo "    </form>\n";
echo "    <form action=\"newncs.php\">\n";
echo "      <input type=\"hidden\" name=\"ncs\" value=\"" . $ncs . "\">\n";
echo "      <input type=\"hidden\" name=\"inc\" value=\"" . $incnum . "\">\n";
echo "      <input type=\"submit\" value=\"Add new NCS\" />\n";
echo "    </form>\n";
echo "  </body>\n</html>\n";
?>