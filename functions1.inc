<?php
/*! \file functions1.inc
 *
 *  \brief Functions used for the application
 *
 *  This file provides a number of functions which are used
 *  throughout the application.  Included are functions for
 *  connecting to the database, displaying database error
 *  messages, displaying page headers and footers, and providing
 *  some standard formatting for specific fields.
 *
 *  \author JJMcD
 *  \date 2012-12-30
 */
/***********************************************************************
 * Software License Agreement
 *
 * Copyright (c) 2013 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the file COPYING for a complete description.
 ***********************************************************************/


//! Get the text associated with the most recent MySQL error
/*! Gets the error message text from MySQL and formats it into a
 *  string for display, including a color indicating the severity.
 *
 * \code
 * Pseucocode:
 * switch ( $type )
 * case $type='error'
 *    $Colour='red'
 *    if ( $prefix is blank )
 *      $prefix = ERROR Message Report
 * case $type='warn'
 *    $Colour='maroon'
 *    if ( $prefix is blank )
 *      $prefix = WARNING Message Report
 * case $type='success'
 *    $Colour='dark green'
 *    if ( $prefix is blank )
 *      $prefix = SUCCESS Report
 * case $type='info' OR anything else
 *    $Colour='navy'
 *    if ( $prefix is blank )
 *      $prefix = INFORMATION Message
 * return a string with HTML for the formatted message
 * \endcode
 *
 *  \param $msg - Message text to display
 *  \param $type - Message severity, default = info
 *  \param $prefix - Prefix for the message, default = empty
 *  \returns formatted string
 */
function getMsg($msg,$type='info',$prefix='')
{
    /*! Color in which to display the message */
  $Colour='';
  switch($type){
  case 'error':
    $Colour='red';
    $prefix = $prefix ? $prefix : _('ERROR') . ' ' ._('Message Report');
    break;
  case 'warn':
    $Colour='maroon';
    $prefix = $prefix ? $prefix : _('WARNING') . ' ' . _('Message Report');
    break;
  case 'success':
    $Colour='darkgreen';
    $prefix = $prefix ? $prefix : _('SUCCESS') . ' ' . _('Report');
    break;
  case 'info':
  default:
    $prefix = $prefix ? $prefix : _('INFORMATION') . ' ' ._('Message');
    $Colour='navy';
  }
  return '<FONT COLOR="' . $Colour . '"><B>' . $prefix . '</B> : ' .$msg . '</FONT>';
}//getMsg


//! Display an error message on the page 
/*! This function calls getMsg and displays the resulting text in a paragraph
 *
 *  \param $msg - Message text to display
 *  \param $type - Message severity, default = info
 *  \param $prefix - Prefix for the message, default = empty
 *  \returns none
 */
function prnMsg($msg,$type='info', $prefix='')
{
  echo '<P>' . getMsg($msg, $type, $prefix) . '</P>';
}//prnMsg


//! Display the top of all pages
/*! Opens the HTML and head section, provides the title to appear
 *  in the titlebar and all of the <head> section, including reference
 *  to the style sheet.  Also writes out the opening <div> and the
 *  page title at the top of the page.
 *
 *  \param $title - Text to appear in the titlebar and page heading
 *  \returns none
 */
function pageHead( $title )
{
  echo "<html><head><title>MCSAR " . $title . "</title>";
  echo "<style type=\"text/css\" media=\"all\">@import \"default1.css\";</style>";
  echo "</head><body>\n";
  echo "<div id=\"headarea\">\n";
  echo "  <h2>" . $title . "</h2>\n";
}//pageHead


//! Display the top of refreshing pages
/*! Opens the HTML and head section, provides the title to appear
 *  in the titlebar and all of the <head> section, including reference
 *  to the style sheet.  Also writes out the opening <div> and the
 *  page title at the top of the page.  This version also instructs
 *  the page to refresh every two minutes.
 *
 *  \param $title - Text to appear in the titlebar and page heading
 *  \returns none
 */
function pageHeadR( $title )
{
  echo "<html><head><title>MCSAR " . $title . "</title>";
  echo "<style type=\"text/css\" media=\"all\">@import \"default1.css\";</style>";
  echo "<meta http-equiv=\"refresh\" content=\"120\" />\n";
  echo "</head><body>\n";
  echo "<div id=\"headarea\">\n";
  echo "  <h2>" . $title . "</h2>\n";
}//pageHeadR

//! Display the top of refreshing pages
/*! Opens the HTML and head section, provides the title to appear
 *  in the titlebar and all of the <head> section, including reference
 *  to the style sheet.  Also writes out the opening <div> and the
 *  page title at the top of the page.  This version also instructs
 *  the focus to a specific field.
 *
 *  \param $title Text to appear in the titlebar and page heading
 *  \param $form Name of form to locate cursor on page open
 *  \param $field Field in form to locate cursor on page open
 *  \returns none
 */
function pageHeadF( $title, $form, $field )
{
  echo "<html>\n";
  echo "  <head>\n";
  echo "    <title>MCSAR " . $title . "</title>\n";
  echo "    <style type=\"text/css\" media=\"all\">@import \"default1.css\";</style>";
  echo "  </head>\n";
  echo "  <body onload=\"document." . $form . "." . 
    $field . ".focus()\">\n";
  echo "    <div id=\"headarea\">\n";
  echo "        <h2>" . $title . "</h2>\n";
}//pageHeadF

/*! Set the background class for team display
 *
 * Looks at the length of time since last contact, team name,
 * and sector to determine what color (class) should be used
 * to display the entry.
 *
 * \li >1500 seconds since contact, class=warn
 * \li >2400 seconds since contact, class=late
 * \li >3600 seconds AND team='NET', class=net
 * \li sector='STAG', class=stage
 *
 * \param $contacttime Time/date of last contact with team
 * \param $sector Sector for the team
 * \param $team Name of team
 * \returns Class name to use for page element
 */
function setBackground( $contacttime, $sector, $team )
{
   /* Class name for page elements */
  $color="";
  $sector=strtoupper(substr($sector,0,4));
  $team=strtoupper(substr($team,0,4));
  if ( $sector == "STAG" )
    {
      $color="stage";
    }
  else
    {
      /* Time of contact */
      $then = strtotime( $contacttime );
      /* Current time */
      $now = mktime();
      /* Number of seconds since contact */
      $seconds = $now - $then;
      // More than 40 minutes, pink background
      if ( $seconds > 2400 ) // 40 minutes
	$color  = "late";
      // More than 25 minutes, lighter pink background
      else if ( $seconds > 1500 ) // 20 minutes
	$color = "warn";
      /* Note whether it is time for a net announcement */
      if ( $row[0] == $team )
	$color = "";
      if ( ($team == "NET") && ($seconds>3600) )
	$color = "net";
    }
  return $color;
}//setBackground

/*! Convert team name to a single letter
 *
 * If the team name is a valid, ICAO phonetic (e.g. Alpha,
 * Bravo, Charlie) convert it to the appropriate single,
 * upper-case letter.
 *
 * \param $team Team name as typed
 * \returns Team letter if phonetic, else left 4 characters upcased
 */
function cleanTeam( $team )
{
  $team=strtoupper($team);
  $phon = array('ALPHA'    => 'A',
	        'BRAVO'    => 'B',
	        'CHARLIE'  => 'C',
	        'DELTA'    => 'D',
	        'ECHO'     => 'E',
	        'FOXTROT'  => 'F',
	        'GOLF'     => 'G',
	        'HOTEL'    => 'H',
	        'INDIA'    => 'I',
	        'JULIET'   => 'J',
	        'KILO'     => 'K',
	        'LIMA'     => 'L',
	        'MIKE'     => 'M',
	        'NOVEMBER' => 'N',
	        'OSCAR'    => 'O',
	        'PAPA'     => 'P',
	        'QUEBEC'   => 'Q',
	        'ROMEO'    => 'R',
	        'SIERRA'   => 'S',
	        'TANGO'    => 'T',
	        'UNIFORM'  => 'U',
	        'VICTOR'   => 'V',
	        'WHISKEY'  => 'W',
	        'X-RAY'    => 'X',
	        'YANKEE'   => 'Y',
	        'ZULU'     => 'Z' );
  if ( $phon[$team] )
    $team=$phon[$team];
  else
    $team = substr($team,0,4);

  return $team;
}

?>
